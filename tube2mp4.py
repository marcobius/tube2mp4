import flet as ft
from pathlib import Path
import yaml
from pytube import YouTube

settings_file = Path("tube2mp4_settings.yaml")

def get_settings():
    settings = {}
    if settings_file.is_file():
        with open(settings_file, 'r') as f:
            settings = yaml.safe_load(f)
    # Happens if the file is empty.
    if settings == None:
        settings = {}
    return settings

def save_settings(settings, page):
    settings['window_width']= page.window_width
    settings['window_height']= page.window_height

    with open(settings_file, 'w+') as f:
        yaml.dump(settings, f)



def main(page: ft.Page):

    settings = get_settings()
    page.title = "Youtube2mp4"
    page.window_width = settings.get('window_width', 600)
    page.window_height = settings.get('window_height', 400.0)
    error = ft.Text()

    def btn_click(e):
        if not url_origen.value:
            url_origen.error_text = "URL del video a baixar..."
        else:
            url = url_origen.value
            # page.clean()
            try:
                yt = YouTube(url)
                try:
                    a = yt.streams.filter(progressive=True, file_extension='mp4').order_by('resolution').desc().first().download()
                except:
                    a = yt.streams.filter(progressive=True).order_by('resolution').desc().first().download()
                print(f"Downloaded {a}")
                error.value=f"Downloaded {a}"
            except Exception as e:
                error.value="Hi ha algun problema amb la url del video. No s'ha pogut baixar."
        page.update()



    # CONTROLS
    page.title = "Pytube frontend"
    url_origen = ft.TextField(label="URL")
    # destino = ft.TextField(label="Destí")
    # # Open directory dialog
    # def get_directory_result(e: ft.FilePickerResultEvent):
    #     destino.value = e.path if e.path else "Cancelled!"
    #     destino.update()

    # get_directory_dialog = ft.FilePicker(on_result=get_directory_result)

    # Save file dialog
    # def save_file_result(e: ft.FilePickerResultEvent):
    #     save_file_path.value = e.path if e.path else "Cancelled!"
    #     save_file_path.update()

    # save_file_dialog = ft.FilePicker(on_result=save_file_result)
    # save_file_path = ft.Text()

    # hide all dialogs in overlay
    # page.overlay.extend([get_directory_dialog, save_file_dialog])
    page.add(
        url_origen,
        ft.ElevatedButton("Download", on_click=btn_click),
        error
        # ft.Row(
        #     [
        #         ft.ElevatedButton(
        #             "Save file",
        #             icon=ft.icons.SAVE,
        #             on_click=lambda _: save_file_dialog.save_file(),
        #             # disabled=page.web,
        #         ),
        #         save_file_path,
        #     ]
        # ),

        # ft.Row(
        # [
        #     destino, 
        #     ft.ElevatedButton("Seleccionar carpeta destí", 
        #                       icon=ft.icons.FOLDER_OPEN, 
        #                       on_click=lambda _: get_directory_dialog.get_directory_path(),
        #                       disabled=page.web)
        # ],
        # ft.Row([ft.ElevatedButton("Paco de Lucia - Entre dos aguas (1976) full video.mp4", icon=ft.icons.DOWNLOAD)])
        # )

    )

    save_settings(settings, page)


# ft.app(target=main, upload_dir="assets/uploads", view=ft.WEB_BROWSER, assets_dir="assets", )
ft.app(target=main, upload_dir="assets/uploads", assets_dir="assets")
